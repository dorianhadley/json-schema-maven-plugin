package io.determan.pojo.generator.schema;

import io.determan.pojo.generator.core.builder.Utility;
import org.mdkt.compiler.InMemoryJavaCompiler;

import java.util.Map;

abstract class AbstractSchemaTest {

    protected final static String PKG = "io.determan.beans";

    protected Map<String, Class<?>> getClasses(String schemaFile) throws Exception {
        return getCompiler(getSources(schemaFile)).compileAll();
    }

    protected Map<String, String> getSources(String schemaFile) throws Exception {
        ParserSchema parser = new ParserSchema(PKG);
        return parser.execute(schemaFile);
    }

    protected InMemoryJavaCompiler getCompiler(Map<String, String> sourceMap) throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();
        for (Map.Entry<String, String> source : sourceMap.entrySet()) {
            compiler.addSource(source.getKey(), source.getValue());
        }
        return compiler;
    }

    protected String getFullClassName(String className) {
        return Utility.className(PKG, className);
    }

    protected void dump(String schemaFile) throws Exception{
        Map<String, String> sourceMap = getSources(schemaFile);
        sourceMap.forEach((s, s2) -> System.out.println(s2));
    }

}
