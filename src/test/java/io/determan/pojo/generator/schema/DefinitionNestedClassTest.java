package io.determan.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DefinitionNestedClassTest extends AbstractSchemaTest {

    @Test
    public void nestedClass() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/definition_nested_class.json");

        Class ParentClass = classMap.get(getFullClassName("ParentClass"));
        Class ChildClass = classMap.get(getFullClassName("ChildClass"));

        Method getter = ParentClass.getDeclaredMethod("getChild", null);
        assertThat(getter).isNotNull();

        Method setter = ParentClass.getDeclaredMethod("setChild", ChildClass);
        assertThat(setter).isNotNull();
    }

}
