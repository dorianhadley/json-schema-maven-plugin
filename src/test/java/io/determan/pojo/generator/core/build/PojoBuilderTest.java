package io.determan.pojo.generator.core.build;

import io.determan.pojo.generator.core.builder.PojoBuilder;
import org.junit.Test;
import org.mdkt.compiler.InMemoryJavaCompiler;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class PojoBuilderTest {

    String pkg = "io.determan.beans";

    @Test
    public void basicClass() throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();

        PojoBuilder builder = new PojoBuilder(pkg, "BasicClass");
        builder.putField("var", String.class);

        Class<?> BasicClass = compiler.compile("io.determan.beans.BasicClass", builder.toString());

        Field[] fields = BasicClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = BasicClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = BasicClass.getDeclaredMethod("getVar", null);
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", String.class);
        assertThat(setter).isNotNull();

        Object basicClass = BasicClass.newInstance();
        setter.invoke(basicClass, "myString");

        assertThat(getter.invoke(basicClass)).isEqualTo("myString");
    }

    @Test
    public void nestedClass() throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();

        PojoBuilder childBuilder = new PojoBuilder(pkg, "ChildClass");
        childBuilder.putField("var", String.class);

        PojoBuilder parentBuild = new PojoBuilder(pkg, "ParentClass");
        parentBuild.putField("child", "ChildClass");

        compiler.addSource("io.determan.beans.ParentClass", parentBuild.toString());
        compiler.addSource("io.determan.beans.ChildClass", childBuilder.toString());

        Map<String, Class<?>> classMap = compiler.compileAll();

        Class ParentClass = classMap.get("io.determan.beans.ParentClass");
        Class ChildClass = classMap.get("io.determan.beans.ChildClass");

        Field[] fields = ParentClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = ParentClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = ParentClass.getDeclaredMethod("getChild", null);
        assertThat(getter).isNotNull();

        Method setter = ParentClass.getDeclaredMethod("setChild", ChildClass);
        assertThat(setter).isNotNull();

        Object childClass = ChildClass.newInstance();
        Object parentClass = ParentClass.newInstance();

        setter.invoke(parentClass, childClass);
        assertThat(getter.invoke(parentClass)).isEqualTo(childClass);

    }


}
