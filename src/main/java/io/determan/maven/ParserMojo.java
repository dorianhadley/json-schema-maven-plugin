package io.determan.maven;

import io.determan.pojo.generator.Parser;
import io.determan.pojo.generator.schema.ParserSchema;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

@Mojo(
        name = "generate",
        defaultPhase = LifecyclePhase.PROCESS_CLASSES,
        requiresDependencyCollection = ResolutionScope.COMPILE,
        requiresDependencyResolution = ResolutionScope.COMPILE
)
public class ParserMojo extends AbstractMojo {

    @Parameter(name = "package", defaultValue = "pkg")
    String pkg;

    @Parameter(defaultValue = "json-schema.json")
    String file;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Parser parser = new ParserSchema(pkg);

        try {
            parser.execute(file);
        } catch (Exception e) {
            throw new MojoFailureException("Failed to process file.", e);
        }

        try {
            parser.write();
        } catch (Exception e) {
            throw new MojoFailureException("Failed to write source files.", e);
        }

    }
}
