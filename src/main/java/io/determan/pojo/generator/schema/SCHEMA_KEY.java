package io.determan.pojo.generator.schema;

public class SCHEMA_KEY {
    public static final String ARRAY_ITEMS = "items";
    public static final String OBJECT_TYPE = "type";
    public static final String OBJECT_NAME = "title";
    public static final String OBJECT_PROPERTIES = "properties";
    public static final String DESCRIPTION = "description";
    public static final String ENUM = "enum";
    public static final String REF = "$ref";
    public static final String DEFINITIONS = "definitions";
    public static final String ALL_OF = "allOf";

}
