package io.determan.pojo.generator.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Resources;
import io.determan.pojo.generator.Parser;
import io.determan.pojo.generator.core.builder.EnumBuilder;
import io.determan.pojo.generator.core.builder.PojoBuilder;
import io.determan.pojo.generator.schema.bean.SchemaClass;
import io.determan.pojo.generator.schema.bean.SchemaEnum;
import io.determan.pojo.generator.schema.bean.SchemaField;
import io.determan.pojo.generator.schema.bean.SchemaObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.net.URL;
import java.util.*;

public class ParserSchema implements Parser {

    private final Log log = LogFactory.getLog(ParserSchema.class);
    private final ObjectMapper mapper = new ObjectMapper();

    private JsonNode root;
    private String pkg;

    List<SchemaObject> objects = new LinkedList<>();

    public ParserSchema() {
    }

    public ParserSchema(String pkg) {
        this.pkg = pkg;
    }

    public Map<String, String> execute(String path) throws Exception {
        URL url = Resources.getResource(path);
        return execute(new File(url.toURI()));
    }

    public Map<String, String> execute(File file) throws Exception {
        root = mapper.readTree(file);

        if(root.get(SCHEMA_KEY.DEFINITIONS) != null) {
            JsonNode definitions = root.get(SCHEMA_KEY.DEFINITIONS);
            Iterator<String> it = definitions.fieldNames();
            while (it.hasNext()) {
                String name = it.next();
                buildStructure(definitions.get(name), name);
            }
        }

        buildStructure(root, null);
        return buildClasses(objects);
    }

    @Override
    public void write() throws Exception {

    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }

    private Map<String, String> buildClasses(List<SchemaObject> objects) throws Exception {
        Map<String, String> sourceMap = new LinkedHashMap<>();
        for (SchemaObject object : objects) {
            if (object instanceof SchemaClass) {
                SchemaClass aClass = (SchemaClass) object;
                PojoBuilder builder = new PojoBuilder(getPkg(), aClass.getName());
                builder.setJavaDocComment(aClass.getDescription());

                aClass.getFields().forEach(field -> builder.putField(field.getName(), field.getType(), field.getDescription()));

                sourceMap.put(builder.getFullClassName(), builder.toString());
            } else if (object instanceof SchemaEnum) {
                SchemaEnum aClass = (SchemaEnum) object;
                EnumBuilder builder = new EnumBuilder(getPkg(), aClass.getName());
                builder.setJavaDocComment(aClass.getDescription());
                aClass.getEnmus().forEach(s -> builder.put(s));
                sourceMap.put(builder.getFullClassName(), builder.toString());
            }
        }
        return sourceMap;
    }


    private List<SchemaObject> buildStructure(JsonNode node, String name) throws Exception {
        String type = parseSimpleTypes(node);
        if(isEnum(node)) {
            objects.add(buildEnumStructure(node, name));
        } else if(SIMPLE_TYPE.TYPE_OBJECT.equals(type)) {
            objects.add(buildClassStructure(node, name));
        }
        return objects;
    }

    private SchemaObject buildClassStructure(JsonNode node, String name) throws Exception {
        parseSimpleName(node, name);
        SchemaClass aClass = new SchemaClass(node, pkg);
        if(hasProperties(node)) {
            JsonNode properties = node.get(SCHEMA_KEY.OBJECT_PROPERTIES);
            Iterator<String> it = properties.fieldNames();
            while (it.hasNext()) {
                String fieldName = it.next();
                JsonNode property = properties.get(fieldName);
                aClass.addField(buildFieldStructure(fieldName, property));
            }
        }

        if (hasAllOf(node)) {
            JsonNode allOfnode = node.get(SCHEMA_KEY.ALL_OF);
            if(isRef(allOfnode)) {
                objects.forEach(schema -> {
                    if (schema instanceof SchemaClass && schema.getName().equals(refSimpleName(allOfnode))) {
                        ((SchemaClass)schema).getFields().forEach(field -> aClass.addField(field));
                    }

                });
            }
        }

        return aClass;
    }

    private SchemaObject buildEnumStructure(JsonNode node, String name) {
        parseSimpleName(node, name);
        return new SchemaEnum(node, pkg);
    }

    private Optional<SchemaField> buildFieldStructure(String name, JsonNode node) throws Exception {

        Optional<SchemaField> optional = Optional.empty();
        SchemaField field = new SchemaField();
        field.setName(name);

        // set type
        String type = parseSimpleTypes(node);

        if (node.get(SCHEMA_KEY.DESCRIPTION) != null) {
            field.setDescription(node.get(SCHEMA_KEY.DESCRIPTION).asText());
        }

        SchemaObject object;
        String classType = SIMPLE_TYPE.getClass(type);
        switch (type) {
            case SIMPLE_TYPE.TYPE_NULL:
                log.warn(String.format("Skipping field [%s] with type[null]. Will not map to java type.", name));
                break;
            case SIMPLE_TYPE.TYPE_ARRAY:
                JsonNode items = node.get("items");
                if (items.isArray()) {
                    log.warn(String.format("Skipping field [%s] with type[array tuples]. Will not map to java type.", name));
                } else {
                    field.setType(SIMPLE_TYPE.getClass(parseSimpleTypes(items)) + "[]");
                    optional = Optional.of(field);
                }
                break;
            case SIMPLE_TYPE.TYPE_OBJECT:

                if(isRef(node)) {
                    field.setType(refSimpleName(node));
                } else {
                    // Build field object
                    buildStructure(node, name);
                    field.setType(parseSimpleName(node, name));
                }

                log.debug(String.format("Adding field [%s] with type [%s]", name, field.getName()));
                optional = Optional.of(field);

                break;
            case SIMPLE_TYPE.TYPE_NUMBER:
            case SIMPLE_TYPE.TYPE_BOOLEAN:
            case SIMPLE_TYPE.TYPE_INTEGER:
            case SIMPLE_TYPE.TYPE_STRING:
                // only support String enum structure as of today.
                if(isEnum(node)) {
                    buildStructure(node, name);
                    classType = parseSimpleName(node, name);
                }

                field.setType(classType);
                log.debug(String.format("Adding field [%s] with type [%s]", name, classType));
                optional = Optional.of(field);
                break;
            default:
                log.warn(String.format("Skipping field [%s] with type [%s]. Will not map to java type.", name, type));
                break;
        }
        return optional;
    }

    private String parseSimpleName(JsonNode node, String name) {
        ObjectNode object = (ObjectNode) node;
        if (object.get(SCHEMA_KEY.OBJECT_NAME) == null && name != null) {
            object.put(SCHEMA_KEY.OBJECT_NAME, name.substring(0, 1).toUpperCase() + name.substring(1));
        }
        return node.get(SCHEMA_KEY.OBJECT_NAME).asText();
    }

    private String parseSimpleTypes(JsonNode node) {
        if(isRef(node)) {
            return SIMPLE_TYPE.TYPE_OBJECT;
        }
        return SIMPLE_TYPE.valueOf(
                node.get(SCHEMA_KEY.OBJECT_TYPE).asText("null").toLowerCase());
    }

    private String refSimpleName(JsonNode node) {
        String refString = node.get(SCHEMA_KEY.REF).asText();
        return parseSimpleName(
                root.at(refString.substring(1)),
                refString.substring(refString.lastIndexOf("/") + 1));
    }
    private boolean isEnum(JsonNode node) {
        String type = parseSimpleTypes(node);
        return SIMPLE_TYPE.TYPE_STRING.equals(type) && node.get(SCHEMA_KEY.ENUM) != null;
    }

    private boolean isRef(JsonNode node) {
        return node.get(SCHEMA_KEY.REF) != null;
    }

    private boolean hasAllOf(JsonNode node) {
        return node.get(SCHEMA_KEY.ALL_OF) != null;
    }

    public boolean hasProperties(JsonNode node) {
        return node.get(SCHEMA_KEY.OBJECT_PROPERTIES) != null;
    }
}
