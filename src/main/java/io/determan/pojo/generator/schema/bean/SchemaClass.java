package io.determan.pojo.generator.schema.bean;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class SchemaClass extends SchemaObject {

    public SchemaClass() {
        super();
    }

    public SchemaClass(JsonNode node, String pkg) {
        super(node, pkg);
    }

    private List<SchemaField> fields = new ArrayList<SchemaField>();

    public List<SchemaField> getFields() {
        return fields;
    }

    public void setFields(List<SchemaField> fields) {
        this.fields = fields;
    }

    public void addField(SchemaField schemaField) {
        fields.add(schemaField);
    }

    public void addField(Optional<SchemaField> schemaField) {
        if (schemaField.isPresent()) {
            fields.add(schemaField.get());
        }
    }
}
