package io.determan.pojo.generator.schema.bean;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class SchemaEnum extends SchemaObject {

    public SchemaEnum() {
        super();
    }

    public SchemaEnum(JsonNode node, String pkg) {
        super(node, pkg);

        JsonNode properties = node.get("enum");
        if (properties != null && properties.isArray()) {
            properties.forEach(n -> addEnum(n.asText()));
        }
    }

    private List<String> enmus = new ArrayList<>();

    public List<String> getEnmus() {
        return enmus;
    }

    public void setEnmus(List<String> enmus) {
        this.enmus = enmus;
    }

    public void addEnum(String value) {
        enmus.add(value);
    }
}
