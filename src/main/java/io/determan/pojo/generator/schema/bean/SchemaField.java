package io.determan.pojo.generator.schema.bean;

public class SchemaField {

    private String name;
    private String type;
    private String description;

    public SchemaField() {
    }

    public SchemaField(String name, Class type) {
        this.name = name;
        this.type = type.getName();
    }

    public SchemaField(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
