package io.determan.pojo.generator;

import java.io.File;
import java.util.Map;

public interface Parser {

    Map<String, String> execute(String path) throws Exception;

    Map<String, String> execute(File file) throws Exception;

    void write() throws Exception ;
}
