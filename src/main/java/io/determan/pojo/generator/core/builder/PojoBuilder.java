package io.determan.pojo.generator.core.builder;

import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.javadoc.Javadoc;
import com.github.javaparser.javadoc.description.JavadocDescription;
import com.github.javaparser.javadoc.description.JavadocInlineTag;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class PojoBuilder extends ClassBuilder {

    public PojoBuilder(String classPackage, String className) {
        super(classPackage, className);
    }

    public void putFields(Map<String, Class> fields) {
        for (Map.Entry<String, Class> entry : fields.entrySet()) {
            putField(entry.getKey(), entry.getValue());
        }
    }

    public void putField(String name, Class type) {
        putField(name, type.getSimpleName(), null);
    }

    public void putField(String name, Class type, String description) {
        putField(name, type.getSimpleName(), description);
    }

    public void putField(String name, String type) {
        putField(name, type, null);
    }

    public void putField(String name, String type, String description) {
        FieldDeclaration field = getDeclaration().addPrivateField(type, name);
        if(StringUtils.isNotBlank(description)) {
            field.setJavadocComment("    ", new Javadoc(JavadocDescription.parseText(description)));
        }

        MethodDeclaration getter = field.createGetter();
        JavadocDescription getterDescription = new JavadocDescription();
        getterDescription.addElement(new JavadocInlineTag("return ", JavadocInlineTag.Type.LINK, ""));
        getter.setJavadocComment("    ", new Javadoc(getterDescription));

        MethodDeclaration setter = field.createSetter();
        JavadocDescription setterDescription = new JavadocDescription();
        setterDescription.addElement(new JavadocInlineTag("param ", null, name));
        setter.setJavadocComment("    ", new Javadoc(setterDescription));

    }
}
