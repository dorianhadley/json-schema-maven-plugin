package io.determan.pojo.generator.core.builder;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import org.eclipse.sisu.space.asm.FieldVisitor;
import org.eclipse.sisu.space.asm.MethodVisitor;

import java.util.EnumSet;

import static org.eclipse.sisu.space.asm.Opcodes.*;
import static org.eclipse.sisu.space.asm.Type.getInternalName;

public class ClassBuilder extends AbstractBuilder<ClassOrInterfaceDeclaration> {

    public ClassBuilder(String classPackage, String className) {
        super(
                new CompilationUnit(classPackage),
                new ClassOrInterfaceDeclaration(EnumSet.of(Modifier.PUBLIC), false, className));

    }


}
